# fi-interview
## Install
```
yarn
```
## Build
```
yarn build
```
## Serve
The `html/` folder will contain a static site which can be served with, for example, [http-server](https://www.npmjs.com/package/http-server).
