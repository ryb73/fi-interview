import React from "react";
import autobind from "class-autobind";
import Walk from "./walk";

export default class Controls extends React.Component {
    constructor(props) {
        super(props);
        autobind(this);

        this.state = { selectedWalk: null };
    }

    render() {
        return <div>{this.renderWalks()}</div>;
    }

    renderWalks() {
        const { mapController, walks } = this.props;
        return walks.map((walk, index) =>
            <Walk key={index} selected={this.isSelected(index)} walk={walk}
                mapController={mapController} onDeselect={this.onDeselect}
                onSelect={() => this.onSelect(index)} />
        );
    }

    isSelected(index) {
        return this.state.selectedWalk === index;
    }

    onSelect(index) {
        const { mapController, walks } = this.props;
        mapController.displayWalk(walks[index]);
        this.setState({ selectedWalk: index });
    }

    onDeselect() {
        const { mapController, walks } = this.props;
        mapController.displayWalks(walks, false);
        this.setState({ selectedWalk: null });
    }
}
