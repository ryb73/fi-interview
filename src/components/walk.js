import React from "react";
import autobind from "class-autobind";
import styled from "styled-components";
import moment from "moment";

const Container = styled.div`
    font-size: 16px;
    font-family: sans-serif;
    font-weight: bold;
    padding: 1em;
    color: #333;
    cursor: pointer;
    background-color: ${backgroundColor}
    &:hover {
        background-color: #EEE;
    }
`;

const Table = styled.table`
    font-weight: normal;
`;

function backgroundColor(props) {
    return props.selected ? "#DDD" : "#FFF";
}

export default class Walk extends React.Component {
    constructor(props) {
        super(props);
        autobind(this);
    }

    render() {
        const { selected, walk: [{ timestamp }] } = this.props;
        return (
            <Container selected={selected} onClick={this.onClick}
                onMouseOver={this.onMouseOver} onMouseOut={this.onMouseOut}
            >
                {timestamp.format("ddd, MMM Do h:mm A")}
                {this.renderDetails()}
            </Container>
        );
    }

    renderDetails() {
        const { selected, walk } = this.props;

        if(!selected)
            return null;

        const start = walk[0];
        const end = walk[walk.length - 1];
        const durationMs = end.timestamp.diff(start.timestamp);
        const distance = this.getDistance();
        const avgSpeed = distance / moment.duration(durationMs).as("hours");

        return (
            <Table>
                <tbody>
                    <tr>
                        <td>Start</td>
                        <td>{start.timestamp.format("h:mm A")}</td>
                    </tr>
                    <tr>
                        <td>End</td>
                        <td>{end.timestamp.format("h:mm A")}</td>
                    </tr>
                    <tr>
                        <td>Duration</td>
                        <td>{moment.utc(durationMs).format("H:mm")}</td>
                    </tr>
                    <tr>
                        <td>Distance</td>
                        <td>{distance.toFixed(2)} mi</td>
                    </tr>
                    <tr>
                        <td>Avg. Speed</td>
                        <td>{avgSpeed.toFixed(2)} mph</td>
                    </tr>
                </tbody>
            </Table>
        );
    }

    getDistance() {
        const distanceCalc = this.props.walk.reduce(({ lastPoint, distance }, point) => {
            return {
                lastPoint: point,
                distance:
                    !lastPoint ? 0
                    : distance + lastPoint.latLng.distanceTo(point.latLng),
            };
        }, { distance: 0 });

        // meters -> miles calculation courtesy of Google
        return distanceCalc.distance * 0.000621371;
    }

    onClick() {
        const { selected, onSelect, onDeselect } = this.props;
        selected ? onDeselect() : onSelect();
    }

    onMouseOver() {
        this.props.mapController.highlightWalk(this.props.walk.id);
    }

    onMouseOut() {
        this.props.mapController.unhighlight();
    }
}