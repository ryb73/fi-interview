import React from "react";
import ReactDOM from "react-dom";
import moment from "moment";
import leaflet from "leaflet";
import rawData from "./data";
import Controls from "./components/controls";
import MapController from "./map-controller";

function isContinuous(point1, point2) {
    return point2.timestamp.diff(point1.timestamp, "minutes") < 30;
}

const walks = rawData
    .map(point => {
        return {
            ...point,
            timestamp: moment(point.timestamp),
            latLng: leaflet.latLng(point.latitude, point.longitude, point.altitude),
        };
    })
    .reduce((acc, point) => {
        if(acc.length === 0)
            return [[point]];

        const lastWalk = acc[acc.length - 1];
        const lastPoint = lastWalk[lastWalk.length - 1];
        if(isContinuous(lastPoint, point))
            lastWalk.push(point);
        else
            acc.push([point]);

        return acc;
    }, []);

walks.forEach((walk, i) => walk.id = i);

const mapController = new MapController();
mapController.displayWalks(walks);

ReactDOM.render(
    <Controls walks={walks} mapController={mapController} />,
    document.getElementById("controls")
);
