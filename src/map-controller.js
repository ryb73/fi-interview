import leaflet from "leaflet";
import autobind from "class-autobind";
import distinctColors from "distinct-colors";

export default class MapController {
    constructor() {
        autobind(this);

        this.map = leaflet.map("map");
        this.pallette = distinctColors({ lightMax: 80 });
        this.colorIndex = 0;
        this.lines = {};

        leaflet.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}", {
            attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
            maxZoom: 18,
            id: "mapbox.streets",
            accessToken: "pk.eyJ1IjoicnliNzMiLCJhIjoiY2p2Yjk0d2thMHNoejRkbm10a2g5cGw5ayJ9.y7Azq2pQ7GFr_Cqkl_kJkQ",
        }).addTo(this.map);
    }

    clear() {
        this.map.eachLayer(layer => {
            if(!(layer instanceof leaflet.TileLayer))
                layer.remove();
        });

        this.lines = {};
        this.colorIndex = 0;
    }

    displayWalk(walk) {
        this.clear();

        const line = this.drawWalk(walk);
        this.map.fitBounds(line.getBounds());

        const coords = walk.map(({ latLng }) => latLng);
        const startMarker = leaflet.marker(coords[0]).addTo(this.map);
        startMarker.bindTooltip("Start").openTooltip();

        const endMarker = leaflet.marker(coords[coords.length - 1]).addTo(this.map);
        endMarker.bindTooltip("End").openTooltip();
    }

    drawWalk(walk) {
        const coords = walk.map(({ latLng }) => latLng);
        return leaflet.polyline(coords).addTo(this.map);
    }

    displayWalks(walks, fitBounds = true) {
        this.clear();

        this.lines = walks.reduce((lines, walk) => {
            const line = this.drawWalk(walk);
            line.setStyle({
                color: this.getNextColor(),
            });

            return { ...lines, [walk.id]: line };
        }, {});

        const coords = walks
            .reduce((acc, walk) => [...acc, ...walk], [])
            .map(({ latLng }) => latLng);

        if(fitBounds)
            this.map.fitBounds(coords);
    }

    highlightWalk(id) {
        Object.values(this.lines).forEach(line =>
            (line === this.lines[id]) ?
                line.setStyle({ opacity: 1 })
            :
                line.setStyle({ opacity: 0.25 })
        );
    }

    unhighlight() {
        Object.values(this.lines).forEach(line => line.setStyle({ opacity: 1 }));
    }

    getNextColor() {
        const color = this.pallette[this.colorIndex];
        this.colorIndex = (this.colorIndex + 1) % this.pallette.length;
        return color;
    }
}