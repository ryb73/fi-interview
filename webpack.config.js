/* eslint-env node */

const path = require("path");

module.exports = {
    mode: "development",
    devtool: "source-map",

    entry:  {
        index: rel("src/index.js"),
    },

    output: {
        path: rel("html/js"),
        filename: "[name].js",
    },

    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader!eslint-loader",
        }],
    },
};

function rel(relPath) {
    return path.resolve(__dirname, relPath);
}
