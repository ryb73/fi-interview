module.exports = {
    env: {
        browser: true,
        commonjs: true,
    },

    parserOptions: {
        ecmaVersion: 2018,
        sourceType: "module",
    },

    extends: [
        "eslint:recommended",
        "plugin:react/recommended",
    ],

    settings: {
        react: {
          version: "detect",
        },
    },

    rules: {
        "linebreak-style": [
            "error",
            "unix"
        ],

        semi: [
            "error",
            "always"
        ],

        "no-unused-vars": [
            "warn",
            { vars: "all", args: "none" }
        ],

        "comma-dangle": [ "warn", "always-multiline" ],

        eqeqeq: [ "error", "always" ],

        "no-console": 0,

        "react/prop-types": false,
    },
};
